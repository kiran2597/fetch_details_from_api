package com.relevel.fetchUser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FetchUserDetails1Application {

	public static void main(String[] args) {
		SpringApplication.run(FetchUserDetails1Application.class, args);
	}

}
