package com.relevel.fetchUser.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.relevel.fetchUser.domain.User;
import com.relevel.fetchUser.persistance.UserRepository;

@Service
public class UserService {

	
	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public void saveUser(User user) {
		userRepository.save(user);
	}

	
	
	@Transactional
	public int loginUser(String username,String password) {
			
		Optional<User> user = userRepository.findByName(username);
		//System.out.println(user.get().getPassword());
		if(user.isPresent() && password.equals(user.get().getPassword())) 
			return 1;
		else if(user.isPresent() && !password.equals(user.get().getPassword()))
			return 0;
		else
			return -1;
	}
	
}
