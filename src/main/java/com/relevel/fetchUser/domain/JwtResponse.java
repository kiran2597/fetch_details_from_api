package com.relevel.fetchUser.domain;

import java.io.Serializable;

public class JwtResponse implements Serializable{
	
	
	private static final long serialVersionUID = 5926468583005150707L;
	private static String jwttoken = "";
	
	
	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}
	
	public static String getJwttoken() {
		return jwttoken;
	}
	
	

}
