package com.relevel.fetchUser.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name="users")
@NoArgsConstructor

public class User implements Serializable{

	
	private static final long serialVersionUID = 5926468583005150707L;
	
	@Id
	@Column
	@GeneratedValue(strategy =  GenerationType.AUTO)
	private long id;
	@NotNull
	@Column
	private String name;
	
	@Size(min = 8)
	@NotNull
	@Column
	private String password;
	
	@Pattern(regexp = "(^$|[0-9]{10})")
	@Column
	private String number;
	
	@Email(message = "Email is Not Valid",regexp = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")
	@NotNull
	@Column(unique = true)
	private String emailId;
	
	@DecimalMin(value="5",inclusive = true)
	@DecimalMax(value="100",inclusive = true)
	@Column
	private int age;
	
	@NotNull
	@Column
	private String gender;
	
	@NotNull
	@Column
	private String address;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
	
	
}
