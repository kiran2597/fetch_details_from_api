package com.relevel.fetchUser.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.relevel.fetchUser.application.UserService;
import com.relevel.fetchUser.domain.User;

@RestController
public class UserController {

	
	@Autowired
	private UserService userService;
	
	
	@PostMapping(value="/user")
	public ResponseEntity<String> saveUser(@RequestBody User user){
		
		try {
			userService.saveUser(user);
			return new ResponseEntity<String>("User Created Successfully",HttpStatus.ACCEPTED); 
		}
		catch(Exception e) {
			return new ResponseEntity<String>("User has provided wrong input",HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
	@RequestMapping(value="/login",method = RequestMethod.POST,consumes = "application/json")
	public ResponseEntity<String> loginUser(@RequestHeader("name") String name,@RequestHeader("password") String password){
	
		
				int result = userService.loginUser(name, password);
				if(result == 1)
					return new ResponseEntity<String>("User has successful logged ",HttpStatus.OK);
				else if(result == 0)
					return new ResponseEntity<String>("Invalid Password",HttpStatus.UNAUTHORIZED);
				return new ResponseEntity<String>("User doesn't exist",HttpStatus.NOT_FOUND);
				
	}
	
}
